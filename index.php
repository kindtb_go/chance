<?php
ini_set('display_errors', 1);

include('Classes/ThrowableObject.php');
include('Classes/Coin.php');
include('Classes/Dice.php');
include('Classes/ObservationResult.php');
include('Classes/Observer.php');


$coin = new Coin();
$dice = new Dice();

$observer = new Observer();
$observer->setObject($coin);

$observer->watch(1000);
$results = $observer->getResults();

?>
<!DOCTYPE html>
<html>
<head>
    <title>Pokemon</title>
</head>
<body>
<table border=1px>
    <?php foreach ($results as $result): ?>
    <tr>
        <td><?php echo $result->getOutcome() ?></td>
        <td><?php echo $result->getNumberOfObservations()?></td>
    </tr>
    <?php endforeach; ?>
</table>
</body>
</html>