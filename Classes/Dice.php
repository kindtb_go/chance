<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 16.09.17
 * Time: 16:10
 */

class Dice extends ThrowableObject{

    public function __construct() {
        $this->setOutcomes(array("1","2","3","4","5","6"));
    }
} 