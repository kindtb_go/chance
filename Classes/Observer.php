<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 16.09.17
 * Time: 16:23
 */

class Observer {
    /** @var ThrowableObject */
    private $object = null;

    /** @var array  */
    private $results = array();

    /**
     * @param ThrowableObject $object
     */
    public function setObject($object) {
        $this->object = $object;
        foreach ($object->getOutcomes() as $outcome) {
            $this->results[$outcome] = 0;
        }
    }

    /**
     * @param int $number
     */
    public function watch($number = 1) {
        for ($i=0; $i<$number; $i++) {
            $this->results[$this->object->throwNow()]++;
        }
    }

    /**
     * @return array
     */
    public function getResults() {
        $results = array();
        foreach ($this->results as $outcome=>$result) {
            $results[] = new ObservationResult($outcome,$result);
        }
        return $results;
    }
} 