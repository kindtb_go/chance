<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 16.09.17
 * Time: 16:10
 */

abstract class ThrowableObject {

    protected $outcomes = array();

    protected function setOutcomes($outcomes) {
        $this->outcomes = $outcomes;
    }
    public function getOutcomes() {
        return $this->outcomes;
    }

    public function throwNow() {
        $numberOfPossibilities = count($this->outcomes);
        $result = rand(0, $numberOfPossibilities-1);  //-1 because max is inclusive
        return $this->outcomes[$result];
    }
} 