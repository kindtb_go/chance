<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 16.09.17
 * Time: 18:43
 */

class ObservationResult {
    private $outcome = '';
    private $numberOfObservations = 0;

    public function __construct($outcome, $numberOfObservations) {
        $this->outcome = $outcome;
        $this->numberOfObservations = $numberOfObservations;
    }

    public function getOutcome() {
        return $this->outcome;
    }
    public function getNumberOfObservations() {
        return $this->numberOfObservations;
    }
} 